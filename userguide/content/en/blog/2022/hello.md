---
title: May 2022 Newsletter 
linkTitle: Hello
date: 2022-24-05
description: Welcome to the Docsy blog!
---

## Steering Comittee

The Steering Comittee have approved Stage 1 of Project X

### Analytics Hub

The Analytics Hub have began work on the [PID](https://gitlab.com/edon-initiative/retrospective-analysis/unsupervisedmethods_adnidata/-/issues/1) for building autoencoders from ADNI data to determine important features from retrospective data.

### Clinic Hub

We have signed a Data Access Agreement with Cohort X

### Upcomng events

The next ARUK conference will take place in London Excel on April 2st....

### Introducing new members

Please give a warm welcome to new members of the Operational Hub

### Contribute to the next Newsletter!

We write our newsletters at the following Gitlab repo....


