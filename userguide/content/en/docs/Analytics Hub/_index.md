---
title: "Analytics Hub"
linkTitle: "Analytics Hub"
weight: 2
aliases:
    - /docs/analytics-hub/
date: 2018-07-30
description: >
  Learn all about the Analytics Hub and ways of working
---

The Analytics Hub is focused on running the analyses that will allow us to make sense of the data collected in the project. It is composed of data scientists and is responsible for developing, validating and refining machine learning ‘fingerprint’ models that can detect the diseases that cause dementia at their earliest stage.

