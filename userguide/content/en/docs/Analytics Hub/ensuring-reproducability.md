---
title: "Ensuring Reproducible Analysis"
linkTitle: "Ensuring Reproducible Analysis"
weight: 9
description: >
  Ensuring Reproducible Research 
---

## How is the Analytics Hub ensuring the algorithms and research behind the EDoN Toolkit?

The Analytics Hub is committed to ensuring that the algorithms behind the toolkit, any experiments and results in any publication can be reproduced and tracked as the algorithms develop. Here are a list of measures that the Analytics Hub implements:

* An external Gitlab is used to track projects workflows and progress
* A hosted Gitlab within the [Turing SRE]() is used to commit all code and analysis pipelines
* We make use of [Continuous Integration / Continuous Development (CI/CD)](https://docs.gitlab.com/ee/ci/) pipelines to automate our analysis and be able to track each experiment
* We are exploring the use of open source/core ML specific experiment tracking and data versioning technology (MLOPS) such as [DVC](https://dvc.org/), [Weights and Biases](https://wandb.ai/site) and [Vetiver](https://vetiver.rstudio.com/)
* We will ensure that any software or algorithms have clear documentation and easily run notebooks that demonstrate core functionality
* All code used to implement the final toolkit and accompanying anonymized data will be deposited within the [AD Workbench](https://www.alzheimersdata.org/ad-workbench) while replicating the analysis environment using containers. Users will be able to reproduce all analyses and rebuild models that are used in the deployed toolkit.
* The Analytics Hub engage in bi-directional feedback with [The Turing Way](https://the-turing-way.netlify.app/welcome) to ensure best practices in data science.