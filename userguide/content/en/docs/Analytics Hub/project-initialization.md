---
title: "Project Initialization"
linkTitle: "Project Initialization"
weight: 9
description: >
  How to submit a proposal that requires resource from the Analytics Hub
---

## How to submit a project proposal to the Analytics Hub?

The Analytics Hub supports a range of work that might include proposing a novel mathematical method to EDoN data, data scoping for a particular cohort, writing publications or organizing events and workshops. To propose a project that requires resource from the Analytics Hub, please follow the process outlined below:

* Populate a [Project Initialization Document (PID)](https://docs.google.com/document/d/1MPOMKVG2nKAZD7N3Zz85y7ddA4j9bl-kKzVg51Hpzbg/edit) that describes the project, work involved and how much resource is required
* Send the **PID** link to Arron Lacey (Senior Community Manager Analytics Hub)
* **Analytics Hub** members will review and make suggestions
* The final PID is then presented and discussed at the bi-weekly **EDoN Scoping Project meeting**
* The **PID** is then sponsored by the Analytics Hub chairs and EDoN Chief Scientific Officer for final approval at the monthly Operational Hub meeting.
* Following approval the project starts!