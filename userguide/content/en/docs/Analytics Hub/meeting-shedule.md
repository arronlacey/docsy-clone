---
title: "Meeting Schedule"
linkTitle: "Meeting Schedule"
weight: 9
description: >
  When does the Analytics Hub meet?
---

## List of meetings

* Fortnightly Internal Analytics Hub meetings on Tuesdays at 1-1.45pm
    * This meeting is an internal Analytics Hub meeting
* Monthly EDoN Analytics Hub meetings on last Friday of the month at 11.30-12.30
    * All EDoN hub members are welcome to this meeting to find out what the Analytics Hub is working on
* EDoN fornightly catch-up every Thursday at 11am 
    * Non-chair representatives of each hub attends to catch other hubs up on progress, report blockers etc