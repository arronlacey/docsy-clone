# Website URL

Please find the EDoN Wiki URL, deployed via [Netlify](app.netlfiy.com) here at [https://snazzy-tarsier-79da2e.netlify.app/](https://snazzy-tarsier-79da2e.netlify.app/)

# Docsy

The EDoN Wiki uses Docsy, a [Hugo](https://gohugo.io) theme for technical documentation sets,
providing simple navigation, site structure, and more. See the [Docsy Getting Started Guide](https://docsy.dev/docs/getting-started/)
for details about the various usage options. 
